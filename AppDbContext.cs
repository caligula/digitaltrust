using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System;

namespace DTSurvey.Data
{


   public class ScenarioDefinition
    {
        public int Id {get;set;}
        public string BaseText {get;set;}
        public string BenefitsText {get;set;}
        public string PrivacyAndSecurityText {get;set;}
        public string ControlText {get;set;}

    }
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }


    // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //         => optionsBuilder
    //             .UseMySql(@"Server=localhost;database=ef;uid=root;pwd=123456;");
         public DbSet<Respondent> Respondents { get; set; }

         public DbSet<PTRespondent> PTRespondents { get; set; }
    }

    public class PTRespondent
    {
        [Key]
        [Required]
        public string Guid {get;set;}
        public DateTime WhenCreated {get;set;}

        public string QueryString {get;set;}

        public int ScenarioId { get; set; }

        //PART1
        public int? Part1FeelProvidesConsiderableBenefits { get; internal set; }
        public int? Part1BelieveCanTakeAdvantage {get;set;}
        public int? Part1ConsiderOfferingValuable {get;set;}
        public int? Part1ExpectToProfitUsingApp {get;set;}
        public int? Part1WouldBeKeenOnUsingApp {get;set;}
        public int? Part1WouldRecommendTheAppToFamilyAndFriends {get;set;}
        public int? Part1ConsidersSharedDataVerySensitive {get;set;}
        public int? Part1ImportantToKeepDataSecure {get;set;}
        public int? Part1ImportantToKeepDataPrivate {get;set;}
        public int? Part1ConsiderSharedDataToRepresentVeryPrivateInformation {get;set;}
        public int? Part1FeelPrivacyViolatedIfDataAccessedByThirdParties {get;set;}
        public int? Part1ImportantToHaveFullControlOverData {get;set;}

        //PART2
        public int? Part2FeelProvidesConsiderableBenefits { get; internal set; }
        public int? Part2BelieveCanTakeAdvantage {get;set;}
        public int? Part2ConsiderOfferingValuable {get;set;}
        public int? Part2ExpectToProfitUsingApp {get;set;}
        public int? Part2WouldBeKeenOnUsingApp {get;set;}
        public int? Part2WouldRecommendTheAppToFamilyAndFriends {get;set;}
        public int? Part2ConsidersSharedDataVerySensitive {get;set;}
        public int? Part2ImportantToKeepDataSecure {get;set;}
        public int? Part2ImportantToKeepDataPrivate {get;set;}
        public int? Part2ConsiderSharedDataToRepresentVeryPrivateInformation {get;set;}
        public int? Part2FeelPrivacyViolatedIfDataAccessedByThirdParties {get;set;}
        public int? Part2ImportantToHaveFullControlOverData {get;set;}
        
        //Part3
        public int? Part3FeelDataIsSecure {get;set;}
        public int? Part3FeelDataIsKeptPrivate {get;set;}
        public int? Part3BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties {get;set;}
        public int? Part3BelieveProviderGuaranteesHighSecurityStandards {get;set;}
        public int? Part3BelieveProviderGuaranteeHighDataConfidentiality {get;set;}
        public int? Part3FeelMyDataIsSafe {get;set;}
        public int? Part3FeelICanTrustProvider {get;set;}
        public int? Part3FeelComfortableUsingTheApp {get;set;}
        public int? Part3BelieveProviderTruthfulWithMe {get;set;}
        public int? Part3BelieveProviderActsWithMyBestInterest {get;set;}
        public int? Part3FeelComfortableSharingPersonalDataWithProvider {get;set;}
        public int? Part3FeelComfortableRecommendingTheProviderToFriendOrFamily {get;set;}
        
       //Part4

        public int? Part4FeelProvideAllowsControlWhoCanAccessData {get;set;}
        public int? Part4BelieveProviderLetUserDecidePurpose {get;set;}
        public int? Part4NoConcernDataUsedMightUsedWayIDontAgreeWith {get;set;}
        public int? Part4NoConcernsThatDataTransferedToThirdPartyWithoutConsent {get;set;}
        public int? Part4FeelThatIHaveControlOverPersonalData {get;set;}
        public int? Part4CanManageWhatHappensToData {get;set;}
        public int? Part4FeelICanTrustProvider {get;set;}
        public int? Part4FeelComfortableUsingApp {get;set;}
        public int? Part4BelieveProviderTruthfulWithMe {get;set;}
        public int? Part4BelieveProviderActsInMyInterest {get;set;}
        public int? Part4FeelComfortableSharingDataWithProvider {get;set;}
        public int? Part4FeelComfortableRecommendingProviderToFriendOrFamily {get;set;}

        //Demographics Part5
        public string Age {get;set;}
        public bool? IsMale {get;set;}
        public string Nationality {get;set;}
        public string Education {get;set;}
        public string Employment {get;set;}
        public bool? UsesDataCollectingApps {get;set;}
        public bool? UsesSimilarApp {get;set;}
        public int? FeelComfortableProvidingData {get;set;}
        public int? WillingnessToTakeRisks {get;set;}

        public string Feedback {get;set;}


        public string LastFinishedStep { get; set; }

        public int SurveyDurationInSeconds { get; set; }

    }

    public class Respondent
    {
        [Key]
        [Required]
        public string Guid {get;set;}
        public DateTime WhenCreated {get;set;}

        public string QueryString {get;set;}

        public int ScenarioId { get; set; }
        public int SurveyDurationInSeconds { get; set; }

       
        public int?	ConsidersSharedDataVerySensitive	{get;set;}
        public int?	ImportantToKeepDataSecure	{get;set;}
        public int?	ImportantToKeepDataPrivate	{get;set;}
        public int?	ConsiderSharedDataToRepresentVeryPrivateInformation	{get;set;}
        public int?	FeelPrivacyViolatedIfDataAccessedByThirdParties	{get;set;}
        public int?	ImportantToHaveFullControlOverData	{get;set;}
        public int?	FeelDataIsSecure	{get;set;}
        public int?	FeelDataIsKeptPrivate	{get;set;}
        public int?	BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties	{get;set;}
        public int?	BelieveProviderGuaranteesHighSecurityStandards	{get;set;}
        public int?	BelieveProviderGuaranteeHighDataConfidentiality	{get;set;}
        public int?	FeelMyDataIsSafe	{get;set;}
        public int?	FeelProvideAllowsControlWhoCanAccessData	{get;set;}
        public int?	BelieveProviderLetUserDecidePurpose	{get;set;}
        public int?	NoConcernDataUsedMightUsedWayIDontAgreeWith	{get;set;}
        public int?	NoConcernsThatDataTransferedToThirdPartyWithoutConsent	{get;set;}
        public int?	FeelThatIHaveControlOverPersonalData	{get;set;}
        public int?	CanManageWhatHappensToData	{get;set;}
        public int?	FeelICanTrustProvider	{get;set;}
        public int?	FeelComfortableUsingApp	{get;set;}
        public int?	BelieveProviderTruthfulWithMe	{get;set;}
        public int?	BelieveProviderActsInMyInterest	{get;set;}
        public int?	FeelComfortableSharingDataWithProvider	{get;set;}
        public int?	FeelComfortableRecommendingProviderToFriendOrFamily	{get;set;}
        public int?	FeelProvidesConsiderableBenefits	{get;set;}
        public int?	BelieveCanTakeAdvantage	{get;set;}
        public int?	ConsiderOfferingValuable	{get;set;}
        public int?	ExpectToProfitUsingApp	{get;set;}
        public int?	WouldBeKeenOnUsingApp	{get;set;}
        public int?	WouldRecommendTheAppToFamilyAndFriends	{get;set;}


        //Demographics
        public string Age {get;set;}
        public bool? IsMale {get;set;}
        public string Nationality {get;set;}
        public string Education {get;set;}
        public string Employment {get;set;}
        public bool? UsesDataCollectingApps {get;set;}
        public bool? UsesSimilarApp {get;set;}
        public int? FeelComfortableProvidingData {get;set;}
        public int? WillingnessToTakeRisks {get;set;}


        public string LastFinishedStep { get; set; }
        
    }
}