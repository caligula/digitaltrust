﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
     
    

    public class PTPart1AnswerModel : PageModel
    {

         private readonly AppDbContext _db;
         [BindProperty]     
        public int? Part1FeelProvidesConsiderableBenefits {get;set;}
         [BindProperty]       
        public int? Part1BelieveCanTakeAdvantage {get;set;}
         [BindProperty]       
        public int? Part1ConsiderOfferingValuable {get;set;}
         [BindProperty]       
        public int? Part1ExpectToProfitUsingApp {get;set;}
         [BindProperty]       
        public int? Part1WouldBeKeenOnUsingApp {get;set;}
         [BindProperty]       
        public int? Part1WouldRecommendTheAppToFamilyAndFriends {get;set;}
         [BindProperty]       
        public int? Part1ConsidersSharedDataVerySensitive {get;set;}
         [BindProperty]       
        public int? Part1ImportantToKeepDataSecure {get;set;}
         [BindProperty]       
        public int? Part1ImportantToKeepDataPrivate {get;set;}
         [BindProperty]       
        public int? Part1ConsiderSharedDataToRepresentVeryPrivateInformation {get;set;}
         [BindProperty]       
        public int? Part1FeelPrivacyViolatedIfDataAccessedByThirdParties {get;set;}
         [BindProperty]       
        public int? Part1ImportantToHaveFullControlOverData {get;set;}


         [BindProperty]       
        public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
         [BindProperty]       
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public PTPart1AnswerModel(AppDbContext db)
        {
            _db = db;
            //InitScenarioDefinitions();

        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "“RatingStar” is a mobile phone app that allows the user to provide his/her opinion on recently purchased products via ratings and comments, independently on where the product has been purchased. Using the app, the user has full access to the anonymised ratings of all other users. ";
            var hfBaseText = "“RatingStar” is a mobile phone app that allows the user to provide his/her opinion on recently purchased products via ratings and comments, independently on where the product has been purchased. Using the app, the user has full access to the anonymised ratings of all other users and collects bonus points that can be redeemed in an online bonus shop.";

            
            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, BenefitsText=""};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =hfBaseText, BenefitsText=""};
            
            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
         
        }

        public IActionResult  OnGet()
        {
            return HandleLoad();
            
        }

        private IActionResult HandleLoad(){
            InitScenarioDefinitions();
            
            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent =  _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    return RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                    {
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                        return Page();
                    }
                    else {
                        Random r = new Random();
                        var scenarioId =  2;//r.Next(1, 2);
                        CurrentScenarioDefinition = ScenarioDefinitions.Single(p=>p.Id==scenarioId);
                        respondent.ScenarioId = CurrentScenarioDefinition.Id;

                        _db.SaveChanges();
                        return Page();   
                    }
                }
                

            } else {
               return RedirectToPage("/Index");
            }
        }
       
        public async Task<IActionResult> OnPostAsync()
        {
            
                if (Part1FeelProvidesConsiderableBenefits ==null || Part1BelieveCanTakeAdvantage==null || Part1ConsiderOfferingValuable==null 
              || Part1ExpectToProfitUsingApp ==null || Part1WouldBeKeenOnUsingApp==null ||Part1WouldRecommendTheAppToFamilyAndFriends==null
              || Part1ConsidersSharedDataVerySensitive ==null || Part1ImportantToKeepDataSecure==null ||Part1ImportantToKeepDataPrivate==null
              || Part1ConsiderSharedDataToRepresentVeryPrivateInformation ==null || Part1FeelPrivacyViolatedIfDataAccessedByThirdParties==null|| Part1ImportantToHaveFullControlOverData==null)
              {
                return HandleLoad();
              }


            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);
            

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {


                respondent.Part1FeelProvidesConsiderableBenefits = Part1FeelProvidesConsiderableBenefits;
                respondent.Part1BelieveCanTakeAdvantage = Part1BelieveCanTakeAdvantage;
                respondent.Part1ConsiderOfferingValuable = Part1ConsiderOfferingValuable;
                respondent.Part1ExpectToProfitUsingApp = Part1ExpectToProfitUsingApp;
                respondent.Part1WouldBeKeenOnUsingApp =Part1WouldBeKeenOnUsingApp;
                respondent.Part1WouldRecommendTheAppToFamilyAndFriends = Part1WouldRecommendTheAppToFamilyAndFriends;
                respondent.Part1ConsidersSharedDataVerySensitive = Part1ConsidersSharedDataVerySensitive;
                respondent.Part1ImportantToKeepDataSecure = Part1ImportantToKeepDataSecure;
                respondent.Part1ImportantToKeepDataPrivate = Part1ImportantToKeepDataPrivate;
                respondent.Part1ConsiderSharedDataToRepresentVeryPrivateInformation =Part1ConsiderSharedDataToRepresentVeryPrivateInformation;
                respondent.Part1FeelPrivacyViolatedIfDataAccessedByThirdParties = Part1FeelPrivacyViolatedIfDataAccessedByThirdParties;
                respondent.Part1ImportantToHaveFullControlOverData = Part1ImportantToHaveFullControlOverData;
                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
               
                        
                respondent.LastFinishedStep = "Part1Answer";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/PTPart2Answer");
        }
    }
}
