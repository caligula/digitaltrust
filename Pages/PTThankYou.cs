﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class PTThankYouModel : PageModel
    {
          private readonly AppDbContext _db;
         
         [BindProperty]
        public string Feedback {get;set;}
        public bool IsFeedbackSent {get;set;}
        public bool ShouldShowGuid {get;set;}
        public string Guid {get;set;}

         public PTThankYouModel(AppDbContext db)
        {
            _db = db;
        }

        public IActionResult OnGet()
        {
              string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");


            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);
            if (respondent!=null)
            {
                IsFeedbackSent = !string.IsNullOrWhiteSpace(respondent.Feedback);
                ShouldShowGuid = ((int)(DateTime.Now - respondent.WhenCreated).TotalSeconds)>3*60;
                Guid = respondent.Guid;
            }
            
            return Page();
        }

        

        public async Task<IActionResult> OnPostAsync()
        {
            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {
                respondent.Feedback = Feedback;
                IsFeedbackSent = true;
                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
                respondent.LastFinishedStep = "ThankYou";
            }


            await _db.SaveChangesAsync();
            return Page();
        }
    }
}
