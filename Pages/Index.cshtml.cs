﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class IndexModel : PageModel
    {
         private readonly AppDbContext _db;
        public bool AlreadyDone {get;set;}
        [BindProperty]
        public string QueryString {get;set;}


        public IndexModel(AppDbContext db)
        {
            _db = db;   
        }
        
        public void OnGet()
        {
            QueryString = Request.QueryString.Value;
            _db.Database.EnsureCreated();
            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                AlreadyDone=true;
                
            }


        }

        public async Task<IActionResult> OnPostAsync(string queryString)
        {      
             var  option = new Microsoft.AspNetCore.Http.CookieOptions();
             option.Expires =  DateTime.Now.AddMinutes(60);  
             
             var guid = Guid.NewGuid().ToString();

            Response.Cookies.Append("guid",guid,option);


            var respondent = new Respondent();
            respondent.Guid = guid;
            respondent.LastFinishedStep = "Start";
            respondent.WhenCreated = DateTime.Now;
            respondent.QueryString = queryString;
            _db.Respondents.Add(respondent);


            await _db.SaveChangesAsync();
            
            return RedirectToPage("/Part1Answer");
            
        }
    }
}
