﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class PTPart3AnswerModel : PageModel
    {

         private readonly AppDbContext _db;

         [BindProperty]       
        public int? Part3FeelDataIsSecure {get;set;}
         [BindProperty]       
        public int? Part3FeelDataIsKeptPrivate {get;set;}
         [BindProperty]       
        public int? Part3BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties {get;set;}
         [BindProperty]       
        public int? Part3BelieveProviderGuaranteesHighSecurityStandards {get;set;}
         [BindProperty]       
        public int? Part3BelieveProviderGuaranteeHighDataConfidentiality {get;set;}
         [BindProperty]       
        public int? Part3FeelMyDataIsSafe {get;set;}
         [BindProperty]       
        public int? Part3FeelICanTrustProvider {get;set;}
         [BindProperty]       
        public int? Part3FeelComfortableUsingTheApp {get;set;}
         [BindProperty]       
        public int? Part3BelieveProviderTruthfulWithMe {get;set;}
         [BindProperty]       
        public int? Part3BelieveProviderActsWithMyBestInterest {get;set;}
         [BindProperty]       
        public int? Part3FeelComfortableSharingPersonalDataWithProvider {get;set;}
         [BindProperty]       
        public int? Part3FeelComfortableRecommendingTheProviderToFriendOrFamily {get;set;}


         public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public PTPart3AnswerModel(AppDbContext db)
        {
            _db = db;
        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "The provider of the previously introduced app “Rating Star” has high data security and privacy standards in place to exclude unauthorized access or unauthorized processing of the data.";
            var hfBaseText = "The provider of the previously introduced app “Rating Star” recently faced issues with data security and privacy violations and had to report cases of unauthorized data access.";

            
            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, BenefitsText=""};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =hfBaseText, BenefitsText=""};
            
            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
         
        }

       public IActionResult OnGet()
        {
            return HandleInit();
            
        }

        public IActionResult HandleInit()
        {
            InitScenarioDefinitions();

            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    return RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                    {
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                        return Page();
                    }   
                    else {
                        Response.Cookies.Delete(cookieValueFromReq);  
                        return RedirectToPage("/Index");
                    }
                }
                

            } else {
                return RedirectToPage("/Index");
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {

                if (Part3FeelDataIsSecure ==null || Part3FeelDataIsKeptPrivate==null || Part3BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties==null 
              || Part3BelieveProviderGuaranteesHighSecurityStandards ==null || Part3BelieveProviderGuaranteeHighDataConfidentiality==null ||Part3FeelMyDataIsSafe==null
              || Part3FeelICanTrustProvider ==null || Part3FeelComfortableUsingTheApp==null ||Part3BelieveProviderTruthfulWithMe==null
              || Part3BelieveProviderActsWithMyBestInterest ==null || Part3FeelComfortableSharingPersonalDataWithProvider==null|| Part3FeelComfortableRecommendingTheProviderToFriendOrFamily==null)
              {
                    return HandleInit();
              }


            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {



                respondent.Part3FeelDataIsSecure = Part3FeelDataIsSecure;
                respondent.Part3FeelDataIsKeptPrivate = Part3FeelDataIsKeptPrivate;
                respondent.Part3BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties = Part3BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties;

                respondent.Part3BelieveProviderGuaranteesHighSecurityStandards = Part3BelieveProviderGuaranteesHighSecurityStandards;
                respondent.Part3BelieveProviderGuaranteeHighDataConfidentiality =Part3BelieveProviderGuaranteeHighDataConfidentiality;
                respondent.Part3FeelMyDataIsSafe = Part3FeelMyDataIsSafe;

                respondent.Part3FeelICanTrustProvider = Part3FeelICanTrustProvider;
                respondent.Part3FeelComfortableUsingTheApp = Part3FeelComfortableUsingTheApp;
                respondent.Part3BelieveProviderTruthfulWithMe = Part3BelieveProviderTruthfulWithMe;

                respondent.Part3BelieveProviderActsWithMyBestInterest =Part3BelieveProviderActsWithMyBestInterest;
                respondent.Part3FeelComfortableSharingPersonalDataWithProvider = Part3FeelComfortableSharingPersonalDataWithProvider;
                respondent.Part3FeelComfortableRecommendingTheProviderToFriendOrFamily = Part3FeelComfortableRecommendingTheProviderToFriendOrFamily;

                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
                respondent.LastFinishedStep = "Part3Answer";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/PTPart4Answer");
        }
    }
}
