﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    
    public class ScenarioModel : PageModel
    {
         private readonly AppDbContext _db;
         
        public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public ScenarioModel(AppDbContext db)
        {
            _db = db;
            InitScenarioDefinitions();
        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "RatingStar is a mobile phone app that allows the user to provide his/her opinion on recently purchased products via ratings and comments, independently on where the product has been purchased. Using the app, the user has full access to the (anonymised) ratings of all other users.";
            var hfBaseText = "HealthFirst is a mobile phone app that allows the user to keep track of his/her physical well-being. The user enters various health data such as his/her detailed medical record, drug usage, blood pressure and physical activity. Using the app, the user receives suggestions on how to improve his/her well-being, for example by proposing exercises or useful nutrition offerings.";

            var rsBenefitTrue = "RatingStar additional benefit: Using the app, the user has full access to the ratings of all other users and collects bonus points that can be redeemed in an online bonus shop.";
            var hfBenefitTrue = "HealthFirst additional benefit: Using the app, the user receives suggestions on how to improve his/her well-being, for example by proposing exercises or useful nutrition offerings, and collects bonus points that can be redeemed in an online bonus shop.";

            var privacyAndSecurityTrue = "The app provider has high data security and privacy standards in place to exclude unauthorized access or unauthorized processing of the data.";
            var privacyAndSecurityFalse = "The app provider recently faced issues with data security and privacy violations and had to report cases of unauthorized data access.";

            var controlTrue = "The app provider allows the user to control exactly for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";
            var controlFalse = "The app provider does not allow the user to control for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";

            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlFalse};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =rsBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlTrue};
            var scenario3 = new ScenarioDefinition() { Id = 3, BaseText =rsBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlFalse};
            var scenario4 = new ScenarioDefinition() { Id = 4, BaseText =rsBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlTrue};
            var scenario5 = new ScenarioDefinition() { Id = 5, BaseText =rsBaseText, BenefitsText=rsBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlFalse};
            var scenario6 = new ScenarioDefinition() { Id = 6, BaseText =rsBaseText, BenefitsText=rsBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlTrue};
            var scenario7 = new ScenarioDefinition() { Id = 7, BaseText =rsBaseText, BenefitsText=rsBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlFalse};
            var scenario8 = new ScenarioDefinition() { Id = 8, BaseText =rsBaseText, BenefitsText=rsBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlTrue};

            //HealthFirst
            var scenario9 = new ScenarioDefinition() { Id = 9, BaseText =hfBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlFalse};
            var scenario10 = new ScenarioDefinition() { Id = 10, BaseText =hfBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlTrue};
            var scenario11 = new ScenarioDefinition() { Id = 11, BaseText =hfBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlFalse};
            var scenario12 = new ScenarioDefinition() { Id = 12, BaseText =hfBaseText, BenefitsText="",PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlTrue};
            var scenario13 = new ScenarioDefinition() { Id = 13, BaseText =hfBaseText, BenefitsText=hfBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlFalse};
            var scenario14 = new ScenarioDefinition() { Id = 14, BaseText =hfBaseText, BenefitsText=hfBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityFalse,ControlText=controlTrue};
            var scenario15 = new ScenarioDefinition() { Id = 15, BaseText =hfBaseText, BenefitsText=hfBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlFalse};
            var scenario16 = new ScenarioDefinition() { Id = 16, BaseText =hfBaseText, BenefitsText=hfBenefitTrue,PrivacyAndSecurityText=privacyAndSecurityTrue,ControlText=controlTrue};

            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
            ScenarioDefinitions.Add(scenario3);
            ScenarioDefinitions.Add(scenario4);
            ScenarioDefinitions.Add(scenario5);
            ScenarioDefinitions.Add(scenario6);
            ScenarioDefinitions.Add(scenario7);
            ScenarioDefinitions.Add(scenario8);
            ScenarioDefinitions.Add(scenario9);
            ScenarioDefinitions.Add(scenario10);
            ScenarioDefinitions.Add(scenario11);
            ScenarioDefinitions.Add(scenario12);
            ScenarioDefinitions.Add(scenario13);
            ScenarioDefinitions.Add(scenario14);
            ScenarioDefinitions.Add(scenario15);
            ScenarioDefinitions.Add(scenario16);
        }

        public void OnGet()
        {

            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent = _db.Respondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                    else {
                        Random r = new Random();
                        var scenarioId =  r.Next(1, 16);
                        CurrentScenarioDefinition = ScenarioDefinitions.Single(p=>p.Id==scenarioId);
                    }
                }
                

            } else {
            
                Random r = new Random();
                var scenarioId =  r.Next(1, 16);
                CurrentScenarioDefinition = ScenarioDefinitions.Single(p=>p.Id==scenarioId);
            }
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.Respondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {
                respondent.ScenarioId = id;
                respondent.LastFinishedStep = "Scenario";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/Step1");
          }
    }
}
