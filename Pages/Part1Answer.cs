﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
     
    

    public class Part1AnswerModel : PageModel
    {

         private readonly AppDbContext _db;
      
         [BindProperty]       
        public int? ConsidersSharedDataVerySensitive {get;set;}
         [BindProperty]       
        public int? ImportantToKeepDataSecure {get;set;}
         [BindProperty]       
        public int? ImportantToKeepDataPrivate {get;set;}
         [BindProperty]       
        public int? ConsiderSharedDataToRepresentVeryPrivateInformation {get;set;}
         [BindProperty]       
        public int? FeelPrivacyViolatedIfDataAccessedByThirdParties {get;set;}
         [BindProperty]       
        public int? ImportantToHaveFullControlOverData {get;set;}

         [BindProperty]       
        public int? FeelDataIsSecure {get;set;}
         [BindProperty]       
        public int? FeelDataIsKeptPrivate {get;set;}
         [BindProperty]       
        public int? BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties {get;set;}
         [BindProperty]       
        public int? BelieveProviderGuaranteesHighSecurityStandards {get;set;}
         [BindProperty]       
        public int? BelieveProviderGuaranteeHighDataConfidentiality {get;set;}

         [BindProperty]       
        public int? FeelMyDataIsSafe {get;set;}
         [BindProperty]       
        public int? FeelProvideAllowsControlWhoCanAccessData {get;set;}
         [BindProperty]       
        public int? BelieveProviderLetUserDecidePurpose {get;set;}
         [BindProperty]       
        public int? NoConcernDataUsedMightUsedWayIDontAgreeWith {get;set;}
         [BindProperty]       
        public int? NoConcernsThatDataTransferedToThirdPartyWithoutConsent {get;set;}
         [BindProperty]       
        public int? FeelThatIHaveControlOverPersonalData {get;set;}
         [BindProperty]       
        public int? CanManageWhatHappensToData {get;set;}
         [BindProperty]       
        public int? FeelICanTrustProvider {get;set;}
         [BindProperty]       
        public int? FeelComfortableUsingApp {get;set;}
         [BindProperty]       
        public int? BelieveProviderTruthfulWithMe {get;set;}
         [BindProperty]       
        public int? BelieveProviderActsInMyInterest {get;set;}
         [BindProperty]       
        public int? FeelComfortableSharingDataWithProvider {get;set;}
         [BindProperty]       
        public int? FeelComfortableRecommendingProviderToFriendOrFamily {get;set;}

         [BindProperty]       
        public int? FeelProvidesConsiderableBenefits { get; set; }
         [BindProperty]       
        public int? BelieveCanTakeAdvantage {get;set;}
         [BindProperty]       
        public int? ConsiderOfferingValuable {get;set;}
         [BindProperty]       
        public int? ExpectToProfitUsingApp {get;set;}
         [BindProperty]       
        public int? WouldBeKeenOnUsingApp {get;set;}
         [BindProperty]       
        public int? WouldRecommendTheAppToFamilyAndFriends {get;set;}

         [BindProperty]       
        public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
         [BindProperty]       
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public Part1AnswerModel(AppDbContext db)
        {
            _db = db;
            //InitScenarioDefinitions();

        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "“RatingStar” is a mobile phone app that allows the user to provide his/her opinion on recently purchased products via ratings and comments, independently on where the product has been purchased. Using the app, the user has full access to the anonymised ratings of all other users and collects bonus points that can be redeemed in an online bonus shop.";
            var hfBaseText = "“HealthFirst” is a mobile phone app that allows the user to keep track of his/her physical well-being. The user enters various health data such as his/her detailed medical record, drug usage, blood pressure and physical activity. Using the app, the user receives suggestions on how to improve his/her well-being, for example by proposing exercises or useful nutrition offerings, and collects bonus points that can be redeemed in an online bonus shop.";

            var privacyAndSecurityTextHi = "The app provider has high data security and privacy standards in place to exclude unauthorized access or unauthorized processing of the data.";
            var privacyAndSecurityTextLow = "The app provider recently faced issues with data security and privacy violations and had to report cases of unauthorized data access.";

            var rsControlTextHi = "RatingStar allows the user to control exactly for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";
            var hfControlTextHi = "HealthFirst allows the user to control exactly for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";
            var rsControlTextLow = "RatingStar does not allow the user to control for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";
            var hfControlTextLow = "HealthFirst does not allow the user to control for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";

            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, PrivacyAndSecurityText=privacyAndSecurityTextHi, ControlText = rsControlTextHi};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =rsBaseText, PrivacyAndSecurityText=privacyAndSecurityTextLow, ControlText = rsControlTextHi};
            var scenario3 = new ScenarioDefinition() { Id = 3, BaseText =rsBaseText, PrivacyAndSecurityText=privacyAndSecurityTextHi, ControlText = rsControlTextLow};
            var scenario4 = new ScenarioDefinition() { Id = 4, BaseText =rsBaseText, PrivacyAndSecurityText=privacyAndSecurityTextLow, ControlText = rsControlTextLow};
            //HealthFirst
            var scenario5 = new ScenarioDefinition() { Id = 5, BaseText =hfBaseText, PrivacyAndSecurityText=privacyAndSecurityTextHi, ControlText = hfControlTextHi};
            var scenario6 = new ScenarioDefinition() { Id = 6, BaseText =hfBaseText, PrivacyAndSecurityText=privacyAndSecurityTextLow, ControlText = hfControlTextHi};
            var scenario7 = new ScenarioDefinition() { Id = 7, BaseText =hfBaseText, PrivacyAndSecurityText=privacyAndSecurityTextHi, ControlText = hfControlTextLow};
            var scenario8 = new ScenarioDefinition() { Id = 8, BaseText =hfBaseText, PrivacyAndSecurityText=privacyAndSecurityTextLow, ControlText = hfControlTextLow};
            
            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
            ScenarioDefinitions.Add(scenario3);
            ScenarioDefinitions.Add(scenario4);
            ScenarioDefinitions.Add(scenario5);
            ScenarioDefinitions.Add(scenario6);
            ScenarioDefinitions.Add(scenario7);
            ScenarioDefinitions.Add(scenario8);
         
        }

        public IActionResult  OnGet()
        {
            return HandleLoad();
            
        }

        private IActionResult HandleLoad(){
            InitScenarioDefinitions();
            
            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent =  _db.Respondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    return RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                    {
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                        return Page();
                    }
                    else {
                        Random r = new Random();
                        var scenarioId =  r.Next(1, 9);
                        CurrentScenarioDefinition = ScenarioDefinitions.Single(p=>p.Id==scenarioId);
                        respondent.ScenarioId = CurrentScenarioDefinition.Id;

                        _db.SaveChanges();
                        return Page();   
                    }
                }
                

            } else {
               return RedirectToPage("/Index");
            }
        }
       
        public async Task<IActionResult> OnPostAsync()
        {
            
                if (ConsidersSharedDataVerySensitive ==null || ImportantToKeepDataSecure==null || ImportantToKeepDataPrivate==null 
              || ConsiderSharedDataToRepresentVeryPrivateInformation ==null || FeelPrivacyViolatedIfDataAccessedByThirdParties==null ||ImportantToHaveFullControlOverData==null
              || FeelDataIsSecure ==null || FeelDataIsKeptPrivate==null ||BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties==null
              || BelieveProviderGuaranteesHighSecurityStandards ==null || BelieveProviderGuaranteeHighDataConfidentiality==null|| FeelMyDataIsSafe==null
              || FeelProvideAllowsControlWhoCanAccessData ==null || BelieveProviderLetUserDecidePurpose==null|| NoConcernDataUsedMightUsedWayIDontAgreeWith==null
              || NoConcernsThatDataTransferedToThirdPartyWithoutConsent ==null || FeelThatIHaveControlOverPersonalData==null|| CanManageWhatHappensToData==null
              || FeelICanTrustProvider ==null || FeelComfortableUsingApp==null|| BelieveProviderTruthfulWithMe==null
              || BelieveProviderActsInMyInterest ==null || FeelComfortableSharingDataWithProvider==null|| FeelComfortableRecommendingProviderToFriendOrFamily==null
              || FeelProvidesConsiderableBenefits ==null || BelieveCanTakeAdvantage==null|| ConsiderOfferingValuable==null
              || ExpectToProfitUsingApp ==null || WouldBeKeenOnUsingApp==null|| WouldRecommendTheAppToFamilyAndFriends==null
              )
              {
                return HandleLoad();
              }


            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.Respondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);
            

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {


                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
               
                respondent.ConsidersSharedDataVerySensitive	=	ConsidersSharedDataVerySensitive;
                respondent.ImportantToKeepDataSecure	=	ImportantToKeepDataSecure;
                respondent.ImportantToKeepDataPrivate	=	ImportantToKeepDataPrivate;
                respondent.ConsiderSharedDataToRepresentVeryPrivateInformation	=	ConsiderSharedDataToRepresentVeryPrivateInformation;
                respondent.FeelPrivacyViolatedIfDataAccessedByThirdParties	=	FeelPrivacyViolatedIfDataAccessedByThirdParties;
                respondent.ImportantToHaveFullControlOverData	=	ImportantToHaveFullControlOverData;
                respondent.FeelDataIsSecure	=	FeelDataIsSecure;
                respondent.FeelDataIsKeptPrivate	=	FeelDataIsKeptPrivate;
                respondent.BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties	=	BelieveDataProtectedAndNotAccessedByUnauthorized3rdParties;
                respondent.BelieveProviderGuaranteesHighSecurityStandards	=	BelieveProviderGuaranteesHighSecurityStandards;
                respondent.BelieveProviderGuaranteeHighDataConfidentiality	=	BelieveProviderGuaranteeHighDataConfidentiality;
                respondent.FeelMyDataIsSafe	=	FeelMyDataIsSafe;
                respondent.FeelProvideAllowsControlWhoCanAccessData	=	FeelProvideAllowsControlWhoCanAccessData;
                respondent.BelieveProviderLetUserDecidePurpose	=	BelieveProviderLetUserDecidePurpose;
                respondent.NoConcernDataUsedMightUsedWayIDontAgreeWith	=	NoConcernDataUsedMightUsedWayIDontAgreeWith;
                respondent.NoConcernsThatDataTransferedToThirdPartyWithoutConsent	=	NoConcernsThatDataTransferedToThirdPartyWithoutConsent;
                respondent.FeelThatIHaveControlOverPersonalData	=	FeelThatIHaveControlOverPersonalData;
                respondent.CanManageWhatHappensToData	=	CanManageWhatHappensToData;
                respondent.FeelICanTrustProvider	=	FeelICanTrustProvider;
                respondent.FeelComfortableUsingApp	=	FeelComfortableUsingApp;
                respondent.BelieveProviderTruthfulWithMe	=	BelieveProviderTruthfulWithMe;
                respondent.BelieveProviderActsInMyInterest	=	BelieveProviderActsInMyInterest;
                respondent.FeelComfortableSharingDataWithProvider	=	FeelComfortableSharingDataWithProvider;
                respondent.FeelComfortableRecommendingProviderToFriendOrFamily	=	FeelComfortableRecommendingProviderToFriendOrFamily;
                respondent.FeelProvidesConsiderableBenefits	=	FeelProvidesConsiderableBenefits;
                respondent.BelieveCanTakeAdvantage	=	BelieveCanTakeAdvantage;
                respondent.ConsiderOfferingValuable	=	ConsiderOfferingValuable;
                respondent.ExpectToProfitUsingApp	=	ExpectToProfitUsingApp;
                respondent.WouldBeKeenOnUsingApp	=	WouldBeKeenOnUsingApp;
                respondent.WouldRecommendTheAppToFamilyAndFriends	=	WouldRecommendTheAppToFamilyAndFriends;
        

                respondent.LastFinishedStep = "Part1Answer";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/Part2Answer");
        }
    }
}
