﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class PTPart2AnswerModel : PageModel
    {

         private readonly AppDbContext _db;
         [BindProperty]       
        public int? Part2FeelProvidesConsiderableBenefits {get;set;}
         [BindProperty]       
        public int? Part2BelieveCanTakeAdvantage {get;set;}
         [BindProperty]       
        public int? Part2ConsiderOfferingValuable {get;set;}
         [BindProperty]       
        public int? Part2ExpectToProfitUsingApp {get;set;}
         [BindProperty]       
        public int? Part2WouldBeKeenOnUsingApp {get;set;}
         [BindProperty]       
        public int? Part2WouldRecommendTheAppToFamilyAndFriends {get;set;}
         [BindProperty]       
        public int? Part2ConsidersSharedDataVerySensitive {get;set;}
         [BindProperty]       
        public int? Part2ImportantToKeepDataSecure {get;set;}
         [BindProperty]       
        public int? Part2ImportantToKeepDataPrivate {get;set;}
         [BindProperty]       
        public int? Part2ConsiderSharedDataToRepresentVeryPrivateInformation {get;set;}
         [BindProperty]       
        public int? Part2FeelPrivacyViolatedIfDataAccessedByThirdParties {get;set;}
         [BindProperty]       
        public int? Part2ImportantToHaveFullControlOverData {get;set;}


         [BindProperty]       
        public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
         [BindProperty]       
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public PTPart2AnswerModel(AppDbContext db)
        {
            _db = db;
        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "“HealthFirst” is a mobile phone app that allows the user to keep track of his/her physical well-being. The user enters various health data such as his/her detailed medical record, drug usage, blood pressure and physical activity. Using the app, the user receives suggestions on how to improve his/her well-being, for example by proposing exercises or useful nutrition offerings, and collects bonus points that can be redeemed in an online bonus shop.";
            var hfBaseText = "“HealthFirst” is a mobile phone app that allows the user to keep track of his/her physical well-being. The user enters various health data such as his/her detailed medical record, drug usage, blood pressure and physical activity. Using the app, the user receives suggestions on how to improve his/her well-being, for example by proposing exercises or useful nutrition offerings. ";

            
            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, BenefitsText=""};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =hfBaseText, BenefitsText=""};
            
            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
         
        }

        public IActionResult OnGet()
        {
            return HandleInit();
            
        }

        public IActionResult HandleInit()
        {
            InitScenarioDefinitions();

            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    return RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                    {
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                        return Page();
                    }   
                    else {
                        Response.Cookies.Delete(cookieValueFromReq);  
                        return RedirectToPage("/Index");
                    }
                }
                

            } else {
                return RedirectToPage("/Index");
            }
        }


        public async Task<IActionResult> OnPostAsync()
        {

                if (Part2FeelProvidesConsiderableBenefits ==null || Part2BelieveCanTakeAdvantage==null || Part2ConsiderOfferingValuable==null 
              || Part2ExpectToProfitUsingApp ==null || Part2WouldBeKeenOnUsingApp==null ||Part2WouldRecommendTheAppToFamilyAndFriends==null
              || Part2ConsidersSharedDataVerySensitive ==null || Part2ImportantToKeepDataSecure==null ||Part2ImportantToKeepDataPrivate==null
              || Part2ConsiderSharedDataToRepresentVeryPrivateInformation ==null || Part2FeelPrivacyViolatedIfDataAccessedByThirdParties==null|| Part2ImportantToHaveFullControlOverData==null)
              {
                  return HandleInit();
              }


            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {


                respondent.Part2FeelProvidesConsiderableBenefits = Part2FeelProvidesConsiderableBenefits;
                respondent.Part2BelieveCanTakeAdvantage = Part2BelieveCanTakeAdvantage;
                respondent.Part2ConsiderOfferingValuable = Part2ConsiderOfferingValuable;
                respondent.Part2ExpectToProfitUsingApp = Part2ExpectToProfitUsingApp;
                respondent.Part2WouldBeKeenOnUsingApp =Part2WouldBeKeenOnUsingApp;
                respondent.Part2WouldRecommendTheAppToFamilyAndFriends = Part2WouldRecommendTheAppToFamilyAndFriends;
                respondent.Part2ConsidersSharedDataVerySensitive = Part2ConsidersSharedDataVerySensitive;
                respondent.Part2ImportantToKeepDataSecure = Part2ImportantToKeepDataSecure;
                respondent.Part2ImportantToKeepDataPrivate = Part2ImportantToKeepDataPrivate;
                respondent.Part2ConsiderSharedDataToRepresentVeryPrivateInformation =Part2ConsiderSharedDataToRepresentVeryPrivateInformation;
                respondent.Part2FeelPrivacyViolatedIfDataAccessedByThirdParties = Part2FeelPrivacyViolatedIfDataAccessedByThirdParties;
                respondent.Part2ImportantToHaveFullControlOverData = Part2ImportantToHaveFullControlOverData;

                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
                respondent.LastFinishedStep = "Part2Answer";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/PTPart3Answer");
        }
    }
}
