﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    
    public class PTPart5AnswerModel : PageModel
    {

         private readonly AppDbContext _db;

         [BindProperty]       
        public string Age {get;set;}
         [BindProperty]       
        public bool? IsMale {get;set;}
         [BindProperty]       
        public string Nationality {get;set;}
         [BindProperty]       
        public string Education {get;set;}
         [BindProperty]       
        public string Employment {get;set;}
         [BindProperty]       
        public bool? UsesDataCollectingApps {get;set;}
         [BindProperty]       
        public bool? UsesSimilarApp {get;set;}
         [BindProperty]       
        public int? FeelComfortableProvidingData {get;set;}
         [BindProperty]       
        public int? WillingnessToTakeRisks {get;set;}

        public PTPart5AnswerModel(AppDbContext db)
        {
            _db = db;
        }


        public async Task<IActionResult> OnPostAsync(int id)
        {


              if (string.IsNullOrWhiteSpace(Age) || IsMale==null || string.IsNullOrWhiteSpace(Nationality) 
              || string.IsNullOrWhiteSpace(Education) || string.IsNullOrWhiteSpace(Employment)  ||UsesDataCollectingApps==null
              || UsesSimilarApp ==null || FeelComfortableProvidingData==null ||WillingnessToTakeRisks==null )
                return Page();


            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {
                respondent.Age = Age;
                respondent.IsMale = IsMale;
                respondent.Nationality = Nationality;
                respondent.Education = Education;
                respondent.Employment = Employment;
                respondent.UsesDataCollectingApps = UsesDataCollectingApps;
                respondent.UsesSimilarApp = UsesSimilarApp;
                respondent.FeelComfortableProvidingData = FeelComfortableProvidingData;
                respondent.WillingnessToTakeRisks = WillingnessToTakeRisks;
                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
                respondent.LastFinishedStep = "Part5";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/PTThankYou");
        }
    }
}
