﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class PTPart4AnswerModel : PageModel
    {

         private readonly AppDbContext _db;
         [BindProperty]       
        public int? Part4FeelProvideAllowsControlWhoCanAccessData {get;set;}
         [BindProperty]       
        public int?  Part4BelieveProviderLetUserDecidePurpose{get;set;}
         [BindProperty]       
        public int? Part4NoConcernDataUsedMightUsedWayIDontAgreeWith {get;set;}
         [BindProperty]
        public int?  Part4NoConcernsThatDataTransferedToThirdPartyWithoutConsent{get;set;}
         [BindProperty]       
        public int?  Part4FeelThatIHaveControlOverPersonalData{get;set;}
         [BindProperty]       
        public int?  Part4CanManageWhatHappensToData{get;set;}
         [BindProperty]       
        public int?  Part4FeelICanTrustProvider{get;set;}
         [BindProperty]       
        public int? Part4FeelComfortableUsingApp {get;set;}
         [BindProperty]       
        public int? Part4BelieveProviderTruthfulWithMe {get;set;}
         [BindProperty]       
        public int? Part4BelieveProviderActsInMyInterest {get;set;}
         [BindProperty]       
        public int? Part4FeelComfortableSharingDataWithProvider {get;set;}
         [BindProperty]       
        public int? Part4FeelComfortableRecommendingProviderToFriendOrFamily {get;set;}

        public List<ScenarioDefinition> ScenarioDefinitions {get;set;}

        
        public ScenarioDefinition CurrentScenarioDefinition {get;set;}
        public PTPart4AnswerModel(AppDbContext db)
        {
            _db = db;
        }

        private void InitScenarioDefinitions()
        {
            var rsBaseText = "The provider of the previously introduced app “Health First” does not allow the user to control for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.";
            var hfBaseText = "The provider of the previously introduced app “Health First” allows the user to control exactly for what purposes (e.g. advertisement or transfer of anonymized data to third parties) the collected data is used.  ";

            
            //RatingStar
            var scenario1 = new ScenarioDefinition() { Id = 1, BaseText =rsBaseText, BenefitsText=""};
            var scenario2 = new ScenarioDefinition() { Id = 2, BaseText =hfBaseText, BenefitsText=""};
            
            ScenarioDefinitions = new List<ScenarioDefinition>();
            ScenarioDefinitions.Add(scenario1);
            ScenarioDefinitions.Add(scenario2);
         
        }

        public IActionResult OnGet()
        {
            return HandleInit();
            
        }

        public IActionResult HandleInit()
        {
            InitScenarioDefinitions();

            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq!=null)
            {
                //if in the cookie use the scenario
                var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

                if (respondent==null)
                {
                    Response.Cookies.Delete(cookieValueFromReq);  
                    return RedirectToPage("/Index");
                }
                else
                {
                    if (respondent.ScenarioId!=0)
                    {
                        CurrentScenarioDefinition = ScenarioDefinitions.SingleOrDefault(p=>p.Id==respondent.ScenarioId);
                        return Page();
                    }   
                    else {
                        Response.Cookies.Delete(cookieValueFromReq);  
                        return RedirectToPage("/Index");
                    }
                }
                

            } else {
                return RedirectToPage("/Index");
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {

                if (Part4FeelProvideAllowsControlWhoCanAccessData ==null || Part4BelieveProviderLetUserDecidePurpose==null || Part4NoConcernDataUsedMightUsedWayIDontAgreeWith==null 
              || Part4NoConcernsThatDataTransferedToThirdPartyWithoutConsent ==null || Part4FeelThatIHaveControlOverPersonalData==null ||Part4CanManageWhatHappensToData==null
              || Part4FeelICanTrustProvider ==null || Part4FeelComfortableUsingApp==null ||Part4BelieveProviderTruthfulWithMe==null
              || Part4BelieveProviderActsInMyInterest ==null || Part4FeelComfortableSharingDataWithProvider==null|| Part4FeelComfortableRecommendingProviderToFriendOrFamily==null)
              {
                   return HandleInit();
              }



            string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");

            var respondent = _db.PTRespondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);

            if (respondent==null)
            {
                Response.Cookies.Delete(cookieValueFromReq);  
                return RedirectToPage("/Index");
            }
            else
            {

                respondent.Part4FeelProvideAllowsControlWhoCanAccessData = Part4FeelProvideAllowsControlWhoCanAccessData;
                respondent.Part4BelieveProviderLetUserDecidePurpose = Part4BelieveProviderLetUserDecidePurpose;
                respondent.Part4NoConcernDataUsedMightUsedWayIDontAgreeWith = Part4NoConcernDataUsedMightUsedWayIDontAgreeWith;
                respondent.Part4NoConcernsThatDataTransferedToThirdPartyWithoutConsent = Part4NoConcernsThatDataTransferedToThirdPartyWithoutConsent;
                respondent.Part4FeelThatIHaveControlOverPersonalData =Part4FeelThatIHaveControlOverPersonalData;
                respondent.Part4CanManageWhatHappensToData = Part4CanManageWhatHappensToData;
                respondent.Part4FeelICanTrustProvider = Part4FeelICanTrustProvider;
                respondent.Part4FeelComfortableUsingApp = Part4FeelComfortableUsingApp;
                respondent.Part4BelieveProviderTruthfulWithMe = Part4BelieveProviderTruthfulWithMe;
                respondent.Part4BelieveProviderActsInMyInterest =Part4BelieveProviderActsInMyInterest;
                respondent.Part4FeelComfortableSharingDataWithProvider = Part4FeelComfortableSharingDataWithProvider;
                respondent.Part4FeelComfortableRecommendingProviderToFriendOrFamily = Part4FeelComfortableRecommendingProviderToFriendOrFamily;

                respondent.SurveyDurationInSeconds = (int)(DateTime.Now - respondent.WhenCreated).TotalSeconds;
                respondent.LastFinishedStep = "Part4Answer";
            }


            await _db.SaveChangesAsync();
            return RedirectToPage("/PTPart5Answer");
        }
    }
}
