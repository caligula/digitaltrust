﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTSurvey.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DTSurvey.Pages
{
    public class Part3ThankYou : PageModel
    {
          private readonly AppDbContext _db;
         
        public bool ShouldShowGuid {get;set;}
        public string Guid {get;set;}

         public Part3ThankYou(AppDbContext db)
        {
            _db = db;
        }

        public IActionResult OnGet()
        {
              string cookieValueFromReq = Request.Cookies["guid"];  
            if (cookieValueFromReq==null)
                return RedirectToPage("/Index");


            var respondent = _db.Respondents.SingleOrDefault(p=>p.Guid == cookieValueFromReq);
            if (respondent!=null)
            {
                ShouldShowGuid = ((int)(DateTime.Now - respondent.WhenCreated).TotalSeconds)>3*60;
                Guid = respondent.Guid;
            }
            
            return Page();
        }

        

       
    }
}
